<?php
/**
 * @file
 * uw_ct_teaching_tip.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_teaching_tip_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'teaching_tips_categories';
  $context->description = 'Display the categories on the sidebar of teaching-tips\' children page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'teaching-resources/teaching-tips/*' => 'teaching-resources/teaching-tips/*',
        'resources/teaching-tips' => 'resources/teaching-tips',
        'search/node' => 'search/node',
        'search/node/*' => 'search/node/*',
        'teaching-tips/by-category/*' => 'teaching-tips/by-category/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-teaching_tips-block' => array(
          'module' => 'views',
          'delta' => 'teaching_tips-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display the categories on the sidebar of teaching-tips\' children page');
  $export['teaching_tips_categories'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'teaching_tips_search';
  $context->description = 'Display tip search on the sidebar of teaching-tips and its childred';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'teaching-resources/teaching-tips/*' => 'teaching-resources/teaching-tips/*',
        'teaching-resources/teaching-tips' => 'teaching-resources/teaching-tips',
        'resources/teaching-tips' => 'resources/teaching-tips',
        'search/node/*' => 'search/node/*',
        'search/node' => 'search/node',
        'teaching-tips/by-category/*' => 'teaching-tips/by-category/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_teaching_tip-search_form_sidebar' => array(
          'module' => 'uw_ct_teaching_tip',
          'delta' => 'search_form_sidebar',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display tip search on the sidebar of teaching-tips and its childred');
  $export['teaching_tips_search'] = $context;

  return $export;
}
