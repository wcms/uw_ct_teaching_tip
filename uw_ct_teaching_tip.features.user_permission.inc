<?php

/**
 * @file
 * uw_ct_teaching_tip.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_teaching_tip_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_teaching_tip content'.
  $permissions['create uw_teaching_tip content'] = array(
    'name' => 'create uw_teaching_tip content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_teaching_tip content'.
  $permissions['delete any uw_teaching_tip content'] = array(
    'name' => 'delete any uw_teaching_tip content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_teaching_tip content'.
  $permissions['delete own uw_teaching_tip content'] = array(
    'name' => 'delete own uw_teaching_tip content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_teaching_tip content'.
  $permissions['edit any uw_teaching_tip content'] = array(
    'name' => 'edit any uw_teaching_tip content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_teaching_tip content'.
  $permissions['edit own uw_teaching_tip content'] = array(
    'name' => 'edit own uw_teaching_tip content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_teaching_tip revision log entry'.
  $permissions['enter uw_teaching_tip revision log entry'] = array(
    'name' => 'enter uw_teaching_tip revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_teaching_tip authored by option'.
  $permissions['override uw_teaching_tip authored by option'] = array(
    'name' => 'override uw_teaching_tip authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_teaching_tip authored on option'.
  $permissions['override uw_teaching_tip authored on option'] = array(
    'name' => 'override uw_teaching_tip authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_teaching_tip promote to front page option'.
  $permissions['override uw_teaching_tip promote to front page option'] = array(
    'name' => 'override uw_teaching_tip promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_teaching_tip published option'.
  $permissions['override uw_teaching_tip published option'] = array(
    'name' => 'override uw_teaching_tip published option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_teaching_tip revision option'.
  $permissions['override uw_teaching_tip revision option'] = array(
    'name' => 'override uw_teaching_tip revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_teaching_tip sticky option'.
  $permissions['override uw_teaching_tip sticky option'] = array(
    'name' => 'override uw_teaching_tip sticky option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_teaching_tip content'.
  $permissions['search uw_teaching_tip content'] = array(
    'name' => 'search uw_teaching_tip content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
