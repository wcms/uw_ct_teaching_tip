<?php
/**
 * @file
 * uw_ct_teaching_tip.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_teaching_tip_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_teaching_tip_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_ct_teaching_tip_user_default_permissions_alter(&$data) {
  if (isset($data['search content'])) {
    $data['search content']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['search content']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
  if (isset($data['use advanced search'])) {
    $data['use advanced search']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['use advanced search']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_teaching_tip_node_info() {
  $items = array(
    'uw_teaching_tip' => array(
      'name' => t('Teaching Tip'),
      'base' => 'node_content',
      'description' => t('A tip to help inform users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
